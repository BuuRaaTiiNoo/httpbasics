import org.apache.http.*;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeaderElementIterator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TestClassss {

    private HttpGet request = new HttpGet("https://yandex.ru/maps/44/izhevsk");
    private CloseableHttpClient client = HttpClients.createDefault();
    private CloseableHttpResponse response = client.execute(request);
    private String csrfToken;

    public TestClassss() throws IOException {
    }

    //Method get yandexuid
    public String getYandexUid() {
        HeaderElementIterator it = new BasicHeaderElementIterator(response.headerIterator("Set-Cookie"));
        while (it.hasNext()) {
            HeaderElement elem = it.nextElement();
            if (elem.getName().equals("yandexuid"))
                return elem.getValue();
        }
        return null;
    }

    //Method get csrfToken
    public String getCsrfToken() throws IOException {

        BufferedReader rd = new BufferedReader(
                new InputStreamReader(response.getEntity().getContent()));

        StringBuffer result = new StringBuffer();
        String line = "";
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }

        String regexp = "csrfToken\":\"[^\"]++";
        Pattern pattern = Pattern.compile(regexp);
        Matcher matcher = pattern.matcher(result);

        while (matcher.find()) {
            csrfToken = matcher.group();
        }
        csrfToken = csrfToken.substring(csrfToken.lastIndexOf("\"") + 1);
        return csrfToken;
    }

    public URI generateURI(String address) throws IOException {
        try {
            return new URIBuilder()
                    .setScheme("https")
                    .setHost("yandex.ru")
                    .setPath("/maps/api/search")
                    .setParameter("text", address)
                    .setParameter("lang", "ru_RU")
                    .setParameter("csrfToken", getCsrfToken())
                    .build();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return null;
    }


    public String getRequest(String address) throws IOException {

        HttpGet request = new HttpGet(generateURI(address));

        request.setHeader("Cookie", "yandexuid=".concat(getYandexUid()));

        HttpResponse response = client.execute(request);

        BufferedReader rd = new BufferedReader(
                new InputStreamReader(response.getEntity().getContent()));

        StringBuffer result = new StringBuffer();
        String line;
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }
        return String.valueOf(result);
    }

    //Method get coordinates
    public String getCoordinates (String address) throws IOException {
        String coordinates = "";
        String regexp = "\"coordinates\":\\[(\\d+.){4}";
        Pattern pattern = Pattern.compile(regexp);
        Matcher matcher = pattern.matcher(getRequest(address));

        while (matcher.find()) {
            coordinates = matcher.group();
        }
        return coordinates;
    }
}
